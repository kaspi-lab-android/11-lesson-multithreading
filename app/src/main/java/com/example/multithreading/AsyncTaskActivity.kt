package com.example.multithreading

import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_async_task.*
import java.util.concurrent.TimeUnit

private const val MAX = 14

class AsyncTaskActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_async_task)
        supportActionBar?.title = getString(R.string.async_task)

        startButton.setOnClickListener {
            val catTask = CatTask()
            catTask.execute()
        }
    }

    inner class CatTask : AsyncTask<Void, Int, Void>() {
        override fun onPreExecute() {
            super.onPreExecute()
            progressBar.isVisible = true
            progressBar.max = MAX
            progressBar.progress = 0
            infoTextView.text = "Начало"
            startButton.visibility = View.INVISIBLE
        }

        override fun doInBackground(vararg params: Void): Void? {
            try {
                var counter = 0
                for (i in 0 until MAX) {
                    getFloor(counter)
                    publishProgress(++counter)
                    if (isCancelled) return null
                }
                TimeUnit.SECONDS.sleep(1)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            infoTextView.text = "Конец"
            startButton.visibility = View.VISIBLE
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            values.getOrNull(0)?.let { value ->
                infoTextView.text = "Этаж: $value"
                progressBar.progress = value
            }
        }

        override fun onCancelled() {
            super.onCancelled()
            infoTextView.text = "Отмена"
            startButton.visibility = View.VISIBLE
            progressBar.isVisible = false
        }

        @Throws(InterruptedException::class)
        private fun getFloor(floor: Int) {
            TimeUnit.SECONDS.sleep(1)
        }
    }
}
