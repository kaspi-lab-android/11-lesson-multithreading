package com.example.multithreading

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_executor.*

class ExecutorActivity : AppCompatActivity() {

    private var executors = Executors()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_executor)

        startButton.setOnClickListener {
            executors.networkIO().execute {
                // download file
                Log.d("app", "downloading...")
                Thread.sleep(2000)

                executors.diskIO().execute {
                    // file processing
                    Log.d("app", "processing...")
                    Thread.sleep(2000)
                    val result = 123 // let's assume this is the result that we get from network

                    executors.mainThread().execute {
                        // main thread
                        Log.d("app", "done...")
                        textView.text = "Done: $result" // get result from io thread
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        executors.shutdown()
        super.onDestroy()
    }
}
