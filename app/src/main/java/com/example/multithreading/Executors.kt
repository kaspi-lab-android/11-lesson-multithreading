package com.example.multithreading

import android.os.Handler
import android.os.Looper
import androidx.annotation.NonNull
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class Executors {
    private var mDiskIO: Executor
    private var mNetworkIO: Executor
    private var mMainThread: Executor

    private constructor(diskIO: Executor, networkIO: Executor, mainThread: Executor) {
        this.mDiskIO = diskIO
        this.mNetworkIO = networkIO
        this.mMainThread = mainThread
    }

    constructor(): this(Executors.newSingleThreadExecutor(), Executors.newFixedThreadPool(3), MainThreadExecutor())

    fun diskIO(): Executor {
        return mDiskIO
    }

    fun networkIO(): Executor {
        return mNetworkIO
    }

    fun mainThread(): Executor {
        return mMainThread
    }

    fun shutdown() {
        (mDiskIO as ExecutorService).shutdown()
        (mNetworkIO as ExecutorService).shutdown()
    }

    private class MainThreadExecutor : Executor {
        private val mainThreadHandler = Handler(Looper.getMainLooper())

        override fun execute(@NonNull command: Runnable) {
            mainThreadHandler.post(command)
        }
    }
}