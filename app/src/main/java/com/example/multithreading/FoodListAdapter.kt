package com.example.multithreading

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_food_list.view.*

class FoodListAdapter(
    private val foodOrderList: MutableList<FoodOrder>,
    private val context: Context) : RecyclerView.Adapter<FoodListAdapter.FoodViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_food_list, parent, false)
        return FoodViewHolder(view)
    }

    override fun getItemCount(): Int {
        return foodOrderList.size
    }

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
        holder.onBind(foodOrderList[position])
    }

    fun getOrderList(): MutableList<FoodOrder> {
        return foodOrderList
    }

    class FoodViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(food: FoodOrder) {
            itemView.foodNameTextView.text = food.foodName
            itemView.foodPriceTextView.text = food.foodPrice.toString()
            itemView.sideOrderTextView.text = food.sideOrder
        }
    }
}
