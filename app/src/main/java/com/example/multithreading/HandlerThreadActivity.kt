package com.example.multithreading

import android.os.*
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_handler_thread.*
import java.lang.ref.WeakReference


class HandlerThreadActivity : AppCompatActivity() {

    // Main thread custom handler class
    private var uiThreadHandler: UiHandler? = null
    // Main thread handler. Pass looper
    private var uiThreadHandler2: Handler? = null
    private var newThread: HandlerThread? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_handler_thread)
        supportActionBar?.title = "HandlerThread"

        if (uiThreadHandler == null) uiThreadHandler = UiHandler(this)

        if (uiThreadHandler2 == null) uiThreadHandler2 = Handler(Looper.getMainLooper())

        // EXAMPLE 1
        startButton1.setOnClickListener {
            // Regular thread
            Thread(Runnable {
                // this is executed on another Thread
                // create a Handler associated with the main Thread
                val handler = Handler(Looper.getMainLooper())

                // post a Runnable to the main Thread
                handler.post {
                    // this is executed on the main Thread
                    Log.d("app", "thread: " + Thread.currentThread().name)
                }
            }).start()
        }

        // EXAMPLE 2
        startButton2.setOnClickListener {
            // create another Thread
            val otherThread = HandlerThread("MyNameIsAnotherThread")
            otherThread.start()
            // create a Handler associated with the other Thread
            val handler = Handler(otherThread.looper)
            // post an event to the other Thread
            handler.post {
                // this is executed on the other Thread
                Log.d("app", "thread: " + otherThread.name)
            }
        }

        // EXAMPLE 3
        startButton3.setOnClickListener {
            val msg = Message()
            msg.obj = "Hello"
            uiThreadHandler?.sendMessage(msg)
        }

        // EXAMPLE 4
        startButton4.setOnClickListener {
            newThread = HandlerThread("MyHandlerThread")
            newThread?.let { thread ->
                thread.start()

                val handler: Handler = object : Handler(thread.looper) {
                    override fun handleMessage(msg: Message) {
                        Log.d("app", "handleMessage " + msg.what.toString() + " in " + Thread.currentThread())
                    }
                }

                for (i in 0..4) {
                    Log.d("app", "sending " + i + " in " + Thread.currentThread())
                    handler.sendEmptyMessageDelayed(i, 3000 + i * 1000L)
                }

                uiThreadHandler2?.post {
                    startButton4.text = "Change title 1"
                }

                uiThreadHandler2?.postDelayed({
                    startButton4.text = "Change title 2"
                }, 2000)
            }
        }
    }

    fun update(str: String) {
        Toast.makeText(this, "update: $str", Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        uiThreadHandler?.removeCallbacksAndMessages(null)
        uiThreadHandler2?.removeCallbacksAndMessages(null)
        uiThreadHandler2 = null
        uiThreadHandler = null
        newThread?.quit()
        super.onDestroy()
    }
}

// static inner class doesn't hold an implicit reference to the outer class
class UiHandler(activity: HandlerThreadActivity) : Handler() {
    // Using a weak reference means you won't prevent garbage collection
    private val myClassWeakReference = WeakReference(activity)

    override fun handleMessage(msg: Message) {
        // process incoming messages here
        // this will run in the thread, which instantiates it
        val activity = myClassWeakReference.get()
        if (activity != null) {
            // do work here
            val k: String = msg.obj as String
            activity.update(k)
        }
    }
}