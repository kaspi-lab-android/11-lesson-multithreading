package com.example.multithreading

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        asyncTaskButton.setOnClickListener {
            startActivity(Intent(this, AsyncTaskActivity::class.java))
        }

        handlerThreadButton.setOnClickListener {
            startActivity(Intent(this, HandlerThreadActivity::class.java))
        }

        orderButton.setOnClickListener {
            startActivity(Intent(this, OrderActivity::class.java))
        }

        executorsButton.setOnClickListener {
            startActivity(Intent(this, ExecutorActivity::class.java))
        }

        rxJavaButton.setOnClickListener {
            startActivity(Intent(this, RxJavaActivity::class.java))
        }
    }
}
