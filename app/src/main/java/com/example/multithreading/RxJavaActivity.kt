package com.example.multithreading

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_rxjava.*

class RxJavaActivity : AppCompatActivity() {

    var disposable = CompositeDisposable()
    var value = 0
    private val serverDownloadObservable: Observable<Int> = Observable.create { emitter ->
        SystemClock.sleep(5000) // simulate delay
        emitter.onNext(5)
        emitter.onComplete()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rxjava)

        exampleButton.setOnClickListener { v ->
            v.isEnabled = false

            val subscribe: Disposable =
                serverDownloadObservable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { integer ->
                            updateTheUserInterface(integer) // this methods updates the ui
                            v.isEnabled = true // enables it again
                        },
                        {
                            Log.e("app", "Error occurred", it)
                        }
                    )

            disposable.add(subscribe)
        }
    }

    private fun updateTheUserInterface(integer: Int) {
        textView.text = integer.toString()
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }
}
